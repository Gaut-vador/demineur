/**
 * @author Gaut-vador <a href="mailto:gauthier.pirlet@tutanota.com?Subject=Demineur" target="_top"></a>
 */
package main;

import game.GameBoard;
import view.Window;

public class Main
{

   public static void main(String[] args)
   {
	  GameBoard gameBoard = new GameBoard(10, 10);

	  Window f = new Window(gameBoard);

	  System.out.println(f.getBoard().toString());
   }
}
