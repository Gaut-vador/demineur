/**
 * @author Gaut-vador
 */

package game;

import java.util.Random;

public class GameBoard
{

   private int _nbBombe;
   private int _size;
   private Case[][] _square;

   /**
    * Constructor of the game, take the _size of the _square and the number of
    * bombes.
    * 
    * @param pSize     Size of the _square
    * @param pNbBombes Number of bombes
    */
   public GameBoard(int pSize, int pNbBombes)
   {
	  if (pSize > 0)
	  {
		 _size = pSize;
	  } else
	  {
		 _size = 10;
	  }

	  _square = new Case[_size][_size];

	  if (pNbBombes > 0 && pNbBombes < (_size * _size))
	  {
		 _nbBombe = pNbBombes;
	  } else
	  {
		 _nbBombe = 10;
	  }
	  initSquare();
	  putBombes();
	  putHint();
   }

   private void initSquare()
   {
	  for (int i = 0; i < _size; i++)
	  {
		 for (int j = 0; j < _size; j++)
		 {
			_square[i][j] = new Case(i, j);
		 }
	  }
   }

   public int getNumberOfCaseToDiscover()
   {
	  return Math.subtractExact((int) Math.pow(_size, 2.0), _nbBombe);
   }

   /**
    * Put the numbers around the bombes as hints.
    */
   private void putHint()
   {
	  for (int i = 0; i < _size; i++)
	  {
		 for (int j = 0; j < _size; j++)
		 {
			if (!_square[i][j].isBombe())
			{
			   _square[i][j].setCaseContent(lookAround(i, j));
			}
		 }
	  }
   }

   /**
    * Return number of bombes around the given position (x and y parameters)
    * 
    * @param x
    * @param y
    * @return number of bombes
    */
   private int lookAround(int x, int y)
   {
	  int cpt = 0;
	  for (int a = -1; a <= 1; a++)
	  {
		 for (int b = -1; b <= 1; b++)
		 {
			if ((a + x >= 0 && a + x < _size) && (b + y >= 0 && b + y < _size))
			{
			   if (_square[a + x][b + y].isBombe())
			   {
				  cpt++;
			   }
			}
		 }
	  }
	  return cpt;
   }

   /**
    * Put the bombes in the field.
    */
   private void putBombes()
   {
	  Random rng = new Random();
	  int x;
	  int y;
	  int cpt = 0;

	  while (cpt < _nbBombe)
	  {
		 if (_square[(x = rng.nextInt(_size))][(y = rng.nextInt(_size))].getCaseContent() != -1)
		 {
			_square[x][y].setCaseContent(-1);
			cpt++;
		 }
	  }
   }

   /**
    * Show bombes with a 'X', and the hints by there number.
    */
   public String toString()
   {
	  String res = "";
	  for (int i = 0; i < _size; i++)
	  {
		 for (int j = 0; j < _size; j++)
		 {
			if (_square[i][j].isBombe())
			{
			   res += 'X';
			} else
			{
			   res += _square[i][j].getCaseContent();
			}
		 }
		 res += "\n";
	  }
	  return res;
   }

   public Case getCell(int i, int j)
   {
	  return _square[i][j];
   }

   public int getNbBombe()
   {
	  return _nbBombe;
   }

   public void setNbBombe(int nbBombe)
   {
	  _nbBombe = nbBombe;
   }

   public int getSize()
   {
	  return _size;
   }

   public void setSize(int size)
   {
	  _size = size;
   }

   public Case[][] getPlateau()
   {
	  return _square;
   }

   public void setPlateau(Case[][] board)
   {
	  _square = board;
   }
}
