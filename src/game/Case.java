package game;

public class Case
{

   private boolean discovered = false;
   private int _xCoord, _yCoord;
   private int _caseContent;

   public Case(int x, int y)
   {
	  _xCoord = x;
	  _yCoord = y;
   }

   public boolean isBombe()
   {
	  return _caseContent == -1;
   }

   public void setCaseContent(int _caseContent)
   {
	  this._caseContent = _caseContent;
   }

   public int getCaseContent()
   {
	  return _caseContent;
   }

   public int getXCoord()
   {
	  return _xCoord;
   }

   public int getYCoord()
   {
	  return _yCoord;
   }

   public boolean isDiscovered()
   {
	  return discovered;
   }

   public void setDiscovered(boolean discovered)
   {
	  this.discovered = discovered;
   }
}
