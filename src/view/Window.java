package view;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;

import game.GameBoard;

/**
 * Main window that contains the game board
 * 
 * @author gaut-vador
 *
 */
public class Window
{

   private JFrame _frame;
   private GameBoard _board;

   /**
    * Create a new window to play.
    * 
    * @param pBoard
    */
   public Window(GameBoard pBoard)
   {

	  _frame = new JFrame("Démineur");
	  _board = pBoard;

	  GameBoardView gameSquare = new GameBoardView(_board);
	  _frame.add(gameSquare, BorderLayout.CENTER);

	  _frame.setSize(new Dimension(500, 500));
	  _frame.setVisible(true);
	  _frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
   }

   public GameBoard getBoard()
   {
	  return _board;
   }

   public void setBoard(GameBoard board)
   {
	  _board = board;
   }
}
