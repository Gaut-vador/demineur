package view;

import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;

import controller.BoardController;
import game.Case;
import game.GameBoard;

public class GameBoardView extends JPanel
{
   private CaseView[][] _caseViewArray;
   private int _size;
   private BoardController controller;

   private static final long serialVersionUID = 87729527401475674L;

   public GameBoardView(GameBoard pSquare)
   {
	  _size = pSquare.getSize();
	  _caseViewArray = new CaseView[_size][_size];
	  this.setLayout(new GridLayout(_size, _size));
	  CaseView currentCase;
	  for (int i = 0; i < _size; i++)
	  {
		 for (int j = 0; j < _size; j++)
		 {
			final Case CASE = pSquare.getCell(i, j);
			currentCase = new CaseView(CASE);
			currentCase.addMouseListener(new MouseListener()
			{

			   @Override
			   public void mouseReleased(MouseEvent e)
			   {
				  // TODO Auto-generated method stub
			   }

			   @Override
			   public void mousePressed(MouseEvent e)
			   {
				  // TODO Auto-generated method stub
			   }

			   @Override
			   public void mouseExited(MouseEvent e)
			   {
				  // TODO Auto-generated method stub
			   }

			   @Override
			   public void mouseEntered(MouseEvent e)
			   {
				  // TODO Auto-generated method stub
			   }

			   @Override
			   public void mouseClicked(MouseEvent e)
			   {
				  controller.buttonPressed(e, CASE);
			   }
			});

			_caseViewArray[i][j] = currentCase;
			this.add(currentCase);
		 }
	  }
	  controller = new BoardController(_caseViewArray, pSquare);
   }

   public int getSquareSize()
   {
	  return _size;
   }

   public CaseView[][] getCaseArray()
   {
	  return _caseViewArray;
   }
}
